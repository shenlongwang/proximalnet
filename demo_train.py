import numpy as np
import sys, os
import mxnet as mx
import time
import logging
from PIL import Image
from collections import namedtuple
from mxnet import optimizer as opt
from mxnet.optimizer import get_updater
from mxnet import metric
from mxnet import misc 
import pdnet 
import math 
import solver
import data_iter

n_epoch = 1000 
b_epoch = 0 
gpu_id = 1
my_ctx = mx.gpu(gpu_id)
n_filters = 48 
n_blocks = 5
clambda = 30 
b_size = 8 
tau = 0.5
filter_size = 7
net = pdnet.primal_dual_net(n_filters, n_blocks, tau, clambda, filter_size)
arg_shapes, out_shapes, aux_shapes = net.infer_shape(data = (b_size, 1, 192, 256))
arg_names = net.list_arguments()
aux_names = net.list_auxiliary_states()

aux_params = {k : nd.zeros(s) for k, s in zip(aux_names, aux_shapes)}
grad_params = {}
arg_params = {}
pdnet_model_prefix = "model_7x7_lambda40"

if (b_epoch > 0):
    model_loaded = mx.model.FeedForward.load(pdnet_model_prefix, b_epoch)
    arg_params = model_loaded.arg_params
else:
    for name, shape in zip(arg_names, arg_shapes):
        # arg_params[name] = mx.random.normal(mean = 0, stdvar = 0.03, shape = shape, ctx=mx.gpu(gpu_id)) 
        if 'weight' in name:
            xavier_std = math.sqrt(2.0 / (1.0*shape[1]*shape[2]*shape[3]))
            print(name, shape, xavier_std)
            arg_params[name] = mx.random.normal(mean = 0, stdvar = xavier_std, shape = shape, ctx=my_ctx) 
        elif 'lambda' in name:
            arg_params[name] = mx.nd.ones(shape = shape, ctx=my_ctx)*clambda*0.02 
        elif 'theta' in name:
            arg_params[name] = mx.nd.ones(shape = shape, ctx=my_ctx) 
        elif 'tau' in name:
            arg_params[name] = mx.nd.ones(shape = shape, ctx=my_ctx)*0.02 
        else:
            print(name, shape, 0.0)
            # arg_params[name] = mx.random.normal(mean = 0, stdvar = 0.02, shape = shape, ctx=my_ctx) 
            arg_params[name] = mx.random.uniform(0, 0.8, shape = shape, ctx=my_ctx) 
        # arg_params[name] = mx.random.uniform(-0.001, 0.001, shape = shape, ctx=mx.gpu(gpu_id)) 

for name, shape in zip(arg_names, arg_shapes):
    if not (name.endswith('data') or name.endswith('label')):
        grad_params[name] = mx.nd.zeros(shape, my_ctx)

train_dataiter = data_iter.FileIter(
        root_dir             = "./data_denoise",
        flist_name           = "train.lst",
        # cut_off_size         = 400,
        rgb_mean             = (113, 113, 113),
        batch_size           = b_size,
        )
val_dataiter = data_iter.FileIter(
        root_dir             = "./data_denoise",
        flist_name           = "val.lst",
        rgb_mean             = (113, 113, 113),
        batch_size           = b_size,
        )

model = solver.Solver(
        ctx                 = my_ctx,
        symbol              = net,
        begin_epoch         = b_epoch,
        num_epoch           = n_epoch,
        arg_params          = arg_params,
        aux_params          = aux_params,
        learning_rate       = 0.002,
        beta1               = 0.9,
        beta2               = 0.999,
        # momentum            = 0.99,
        # clip_gradient       = 1.0,
        lr_scheduler        = misc.FactorScheduler(100000,0.5),
        wd                  = 0.0001)

model.fit(
        train_data          = train_dataiter,
        eval_data           = val_dataiter,
        batch_end_callback  = mx.callback.Speedometer(1, 10),
        epoch_end_callback  = mx.callback.do_checkpoint(pdnet_model_prefix),
        log_file            = 'train_'+pdnet_model_prefix+'.log'
        # epoch_end_callback  = lr_callback(model)
        )
