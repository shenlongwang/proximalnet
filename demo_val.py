import numpy as np
import mxnet as mx
import sys, os
import time
from PIL import Image
from collections import namedtuple
import matplotlib.pyplot as plt
import pdnet_single_loss as pdnet 
# import pdnet 
import scipy.io 
import math 
import cv2 
import solver
import data_iter
import argparse

parser = argparse.ArgumentParser(description='Parses model prefix and do validation experiment')
parser.add_argument('--prefix', type=str,default="model_7x7_lambda40",
                    help='the model prefix')
parser.add_argument('--epoch', type=int,default=400,
                    help='epoch')
args = parser.parse_args()

b_epoch = args.epoch 
pdnet_model_prefix = args.prefix 

def read_depth(filename, h, w):
    with open(filename, 'rb') as f:
        data = np.fromfile(f, np.float32, count=w*h)
        depth = np.resize(data, (w, h))
        depth = np.transpose(depth) 
        return depth
    print("Could not open %s file" % (filename))
    return NULL 
def read_img(root_dir, img1_name, label_name):
    data_buffer = np.fromfile(os.path.join(root_dir, img1_name), dtype=np.int16)
    noisy = data_buffer[2:]
    noisy = noisy.reshape((data_buffer[0], data_buffer[1]))
    noisy = np.clip(noisy.astype(np.float32)/5000, 0, 3)
    im_origin = noisy
    
    data_buffer = np.fromfile(os.path.join(root_dir, label_name), dtype=np.int16)
    clean = data_buffer[2:]
    clean = clean.reshape((data_buffer[0], data_buffer[1]))
    clean = np.clip(clean.astype(np.float32)/5000, 0, 3)
    img = clean 
    sz = clean.shape
    
    # print(clean.shape)
    
    clean = np.expand_dims(clean, axis=2)  # (1, c, h, w)
    clean = np.swapaxes(clean, 0, 2)
    clean = np.swapaxes(clean, 1, 2)  # (c, h, w)
    clean = np.expand_dims(clean, axis=0)  # (1, c, h, w)
    
    
    noisy = np.expand_dims(noisy, axis=2)  # (1, c, h, w)
    noisy = np.swapaxes(noisy, 0, 2)
    noisy = np.swapaxes(noisy, 1, 2)  # (c, h, w)
    noisy = np.expand_dims(noisy, axis=0)  # (1, c, h, w)
    return (im_origin, noisy, img)

n_epoch = 400 
gpu_id = 0
ctx = mx.gpu(gpu_id)


root_dir             = "./data_denoise"
flist_name           = os.path.join(root_dir, "val.lst")

output_dir           = "./output_denoise"

num_data = len(open(flist_name, 'r').readlines())
f = open(flist_name, 'r')
denoisenet, denoisenet_args, denoisenet_auxs = mx.model.load_checkpoint(pdnet_model_prefix, b_epoch)

rmse = np.zeros((num_data, 5)) 
psnr = np.zeros((num_data, 5))

c_max = 0
for x in xrange(num_data): 
# for x in xrange(num_data): 
    _, data_img_name, label_img_name = f.readline().strip('\n').split(" ")
    print(data_img_name, label_img_name)
    (im_origin, data, gt) = read_img(root_dir, data_img_name, label_img_name)
    c_max = max(c_max, gt.max())
    print(c_max)
    denoisenet_args["data"] = mx.nd.array(data, ctx)
    data_shape = denoisenet_args["data"].shape
    label_shape = data_shape
    denoisenet_args["loss1_label"] = mx.nd.empty(label_shape, ctx)
    denoisenet_args["loss2_label"] = mx.nd.empty(label_shape, ctx)
    denoisenet_args["loss3_label"] = mx.nd.empty(label_shape, ctx)
    denoisenet_args["loss4_label"] = mx.nd.empty(label_shape, ctx)
    denoisenet_args["loss5_label"] = mx.nd.empty(label_shape, ctx)
    # denoisenet_args["loss6_label"] = mx.nd.empty(label_shape, ctx)
    # denoisenet_args["loss7_label"] = mx.nd.empty(label_shape, ctx)
    t = time.time()
    exector = denoisenet.bind(ctx, denoisenet_args ,args_grad=None, grad_req="null", aux_states=denoisenet_args)
    exector.forward(is_train=False)
    for p in xrange(5): 
        output = exector.outputs[p].asnumpy()
        print('Time %2.5f' % (time.time()-t))
        output = output.reshape(data_shape[0], data_shape[2], data_shape[3])
        pred = np.swapaxes(output, 0, 1)
        pred = np.swapaxes(pred, 1, 2)  # (c, h, w)
        pred = np.clip(pred, 0, 1)
        pred = pred.reshape(pred.shape[0], pred.shape[1]) 
        rmse[x, p] = math.sqrt(((pred*5000 - gt*5000) ** 2).mean(axis = None))
        psnr[x, p] = 20*math.log10(gt.max()*5000/rmse[x, p])
        print('RMSE:%2.4f, PSNR:%2.4f' % (rmse[x, p], psnr[x, p]))
    # # print(im_origin.shape, gt.shape, pred.shape)
    # result = np.concatenate((im_origin, gt, pred), axis = 0)
    # result = Image.fromarray((result * 255).astype(np.uint8))
    # output_file = os.path.join(output_dir, label_img_name)  
    # # print(output_file[:-3]+'png')
    # result.save(output_file[:-3]+'png')

for p in xrange(5):
    print('Iter %d: Mean RMSE:%2.4f, mean PSNR:%2.4f'% (p, rmse[:, p].mean(axis = None), psnr[:, p].mean(axis = None)))
scipy.io.savemat('result_singleloss.mat', dict(rmse=rmse, psnr=psnr))
# scipy.io.savemat('result_val_per_iter.mat', dict(rmse=rmse, psnr=psnr))
