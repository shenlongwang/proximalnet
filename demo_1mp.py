import numpy as np
import mxnet as mx
import sys, os
import time
from PIL import Image
from collections import namedtuple
import matplotlib.pyplot as plt
import pdnet 
import scipy.io 
import math 
import cv2 
import solver
import data_iter
import argparse

parser = argparse.ArgumentParser(description='Parses model prefix and do validation experiment')
parser.add_argument('--prefix', type=str,default="model_5x5",
                    help='the model prefix')
parser.add_argument('--epoch', type=int,default=10,
                    help='epoch')
args = parser.parse_args()

b_epoch = args.epoch 
pdnet_model_prefix = args.prefix 

def read_depth(filename, h, w):
    with open(filename, 'rb') as f:
        data = np.fromfile(f, np.float32, count=w*h)
        depth = np.resize(data, (w, h))
        depth = np.transpose(depth) 
        return depth
    print("Could not open %s file" % (filename))
    return NULL 
def read_img(root_dir, img1_name, label_name):
    clean = Image.open(os.path.join(root_dir, img1_name))
    clean = np.array(clean, dtype=np.float32)/255.0  # (h, w, c)
    sz = clean.shape
    if (sz[0] > sz[1]):
        clean = np.swapaxes(clean, 0, 1)
    clean= np.dot(clean[...,:3], [0.299, 0.587, 0.114])
    # clean= cv2.resize(clean, (2000, 2000))
    img = clean
    clean = np.expand_dims(clean, axis = 2)
    sigma = 25.0 / 255.0
    noisy = clean + sigma * np.random.randn(clean.shape[0], clean.shape[1], clean.shape[2])
    # noisy = np.clip(np.floor(noisy*255)/255.0, 0, 1)
    clean = np.swapaxes(clean, 0, 2)
    clean = np.swapaxes(clean, 1, 2)  # (c, h, w)
    clean = np.expand_dims(clean, axis=0)  # (1, c, h, w)
    clean = np.swapaxes(clean, 0, 1)  # (c, h, w)
    
    noisy = np.swapaxes(noisy, 0, 2)
    noisy = np.swapaxes(noisy, 1, 2)  # (c, h, w)
    noisy = np.expand_dims(noisy, axis=0)  # (1, c, h, w)
    noisy = np.swapaxes(noisy, 0, 1)  # (c, h, w)
    # print clean.shape
    # print noisy.shape
    return (clean, noisy, img)

n_epoch = 400 
gpu_id = 0
ctx = mx.gpu(gpu_id)


root_dir             = "./data_denoise"
flist_name           = os.path.join(root_dir, "val.lst")

output_dir           = "./output_denoise"

num_data = len(open(flist_name, 'r').readlines())
f = open(flist_name, 'r')
denoisenet, denoisenet_args, denoisenet_auxs = mx.model.load_checkpoint(pdnet_model_prefix, b_epoch)

rmse = np.zeros(num_data) 
psnr = np.zeros(num_data) 
for x in xrange(num_data): 
# for x in xrange(num_data): 
    _, data_img_name, label_img_name = f.readline().strip('\n').split(" ")
    print(data_img_name, label_img_name)
    (im_origin, data, gt) = read_img(root_dir, data_img_name, label_img_name)
    t = time.clock()
    denoisenet_args["data"] = mx.nd.array(data, ctx)
    data_shape = denoisenet_args["data"].shape
    label_shape = data_shape
    print(label_shape)
    denoisenet_args["loss1_label"] = mx.nd.empty(label_shape, ctx)
    denoisenet_args["loss2_label"] = mx.nd.empty(label_shape, ctx)
    denoisenet_args["loss3_label"] = mx.nd.empty(label_shape, ctx)
    denoisenet_args["loss4_label"] = mx.nd.empty(label_shape, ctx)
    denoisenet_args["loss5_label"] = mx.nd.empty(label_shape, ctx)
    # denoisenet_args["loss6_label"] = mx.nd.empty(label_shape, ctx)
    # denoisenet_args["loss7_label"] = mx.nd.empty(label_shape, ctx)
    
    t = time.clock()
    denoisenet_args["data"] = mx.nd.array(data, ctx)
    exector = denoisenet.bind(ctx, denoisenet_args ,args_grad=None, grad_req="null", aux_states=denoisenet_args)
    for x in xrange(50): 
        exector.forward(is_train=False)
        output = exector.outputs[4].asnumpy()
    print('Time %2.5f' % (time.clock()-t))
    output = output.reshape(data_shape[0], data_shape[2], data_shape[3])
    pred = np.swapaxes(output, 0, 1)
    pred = np.swapaxes(pred, 1, 2)  # (c, h, w)
    pred = np.clip(pred, 0, 1)
    pred = pred.reshape(pred.shape[0], pred.shape[1]) 
    rmse[x] = math.sqrt(((pred - gt) ** 2).mean(axis = None))
    psnr[x] = 20*math.log10(1/rmse[x])
    result = Image.fromarray((pred * 255).astype(np.uint8))
    output_file = os.path.join(output_dir, label_img_name)  
    print(output_file[:-3]+'png')
    # result.save(output_file[:-3]+'png')
    print('RMSE:%2.4f, PSNR:%2.4f' % (rmse[x], psnr[x]))

print('Mean RMSE:%2.4f, mean PSNR:%2.4f'% (rmse.mean(axis = None), psnr.mean(axis = None)))
scipy.io.savemat('result_val_trial1.mat', dict(rmse=rmse, psnr=psnr))
