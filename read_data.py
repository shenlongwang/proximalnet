import sys, os
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import cv2

def read_depth(filename, h, w):
    with open(filename, 'rb') as f:
        data = np.fromfile(f, np.float32, count=w*h)
        depth = np.resize(data, (w, h))
        depth = np.transpose(depth)
        return depth
    print("Could not open %s file" % (filename))
    return 0

def read_rgbd(folder, rgb_file, depth_file):
    rgb = Image.open(os.path.join(folder, rgb_file))
    rgb = np.array(rgb, dtype=np.float32) / 255.0
    # rgb = scipy.misc.imresize(rgb, (384, 512))
    depth = read_depth(os.path.join(folder, depth_file), rgb.shape[0], rgb.shape[1])
    rgb = cv2.resize(rgb, (512,384))
    depth = cv2.resize(depth, (512,384))
    print(np.mean(depth))
    print(depth.shape)
    plt.subplot(1, 2, 1)
    plt.imshow(rgb)
    plt.subplot(1, 2, 2)
    plt.imshow(depth)
    plt.show()

if __name__ == "__main__":
    read_rgbd('./data_depth', 'im_0001.png', 'depth_0001.bin')
    # read_rgbd('/ais/gobi4/slwang/depth/data_depth', 'im_0001.png', 'depth_0001.bin')
